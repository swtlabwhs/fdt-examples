#!/usr/bin/python

import sys

current_word = None
current_count = 1

for line in sys.stdin:
    word, count = line.strip().split(',')
    if current_word:
        if word == current_word:
            current_count += int(count)
        else:
            print current_word + "," + str(current_count)
            current_count = 1

    current_word = word

if current_count > 1:
    current_word + "," + str(current_count)